import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Url)

def penghasilan1 = WebUI.setText(findTestObject('Page_Hitung_Harga/Input_Penghasilan'), penghasilan)

def pengeluaran1 = WebUI.setText(findTestObject('Page_Hitung_Harga/Input_Pengeluaran'), pengeluaran)

WebUI.click(findTestObject('Page_Hitung_Harga/Jangka_Waktu'))

WebUI.selectOptionByValue(findTestObject('Page_Hitung_Harga/Jangka_Waktu'), waktu, true)

WebUI.click(findTestObject('Object Repository/Page_Hitung_Harga/button_Hitung'))

WebUI.takeScreenshot()

def penghasilanValue = Float.parseFloat(penghasilan)

def pengeluaranValue = Float.parseFloat(pengeluaran)

def JangkaWaktu = Float.parseFloat(waktu)

def hasil_perhitungan = (((penghasilanValue - pengeluaranValue) * JangkaWaktu) * 12) / 3

println('Total Hasil perhitungan: ' + String.format('%.0f', hasil_perhitungan))

WebUI.closeBrowser()

